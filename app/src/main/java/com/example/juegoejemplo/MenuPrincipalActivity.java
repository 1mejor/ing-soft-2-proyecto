package com.example.juegoejemplo;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.example.juegoejemplo.Datos.DbProccess;
import com.example.juegoejemplo.Entidades.Preguntas;
import com.example.juegoejemplo.Entidades.Usuarios;
import com.example.juegoejemplo.Services.ApiService;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuPrincipalActivity extends AppCompatActivity {
        Button jugarBtn, crearReglasBtn, mantenimientoPreguntasBtn, rankingBtn, gestionarUsuarios;
    String usrType = "";
    DbProccess _db;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_menu_principal);
            _db = new DbProccess(getApplicationContext());
            InicializarControladores();
            ObtenerTipoUsuario();
        }

        private void InicializarControladores() {
            jugarBtn = (Button) findViewById(R.id.jugarBtn);
            crearReglasBtn = (Button) findViewById(R.id.crearReglasBtn);
            mantenimientoPreguntasBtn = (Button) findViewById(R.id.mantenimientoPreguntasBtn);
            rankingBtn = (Button) findViewById(R.id.rankingBtn);
            gestionarUsuarios = (Button) findViewById(R.id.gestionarUsuarios);
        }

    private void ObtenerTipoUsuario() {
        Call<String> response = ApiService.getApiService().getAcceso(_db.ObtenerUsuarioSession().getId());
        response.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()){
                    usrType = response.body();
                    AjustarSegunUsuario();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                _db.CerrarSession();
                finish();
            }
        });
    }

        private void AjustarSegunUsuario() {
            //usar sql.
            Intent i = getIntent();
            if(usrType.isEmpty()) {
                _db.CerrarSession();
                Toast.makeText(this,"Cerrando sesion, no se pudo encontrar tipo de usuario en el servidor.",Toast.LENGTH_LONG).show();
                finish();
            }
//            HashMap<?, ?> values = (HashMap<?, ?>) UsuarioQuery.Buscar(getApplicationContext(), new String[]{"type"}, i.getStringExtra("cedula"));
            try {
                switch (usrType) {
                    case "1":
                        jugarBtn.setVisibility(View.GONE);
                        crearReglasBtn.setVisibility(View.GONE);
                        mantenimientoPreguntasBtn.setVisibility(View.GONE);
                        rankingBtn.setVisibility(View.GONE);
                        gestionarUsuarios.setVisibility(View.VISIBLE);
                        break;
                    case "2":
                        jugarBtn.setVisibility(View.GONE);
                        crearReglasBtn.setVisibility(View.VISIBLE);
                        mantenimientoPreguntasBtn.setVisibility(View.VISIBLE);
                        rankingBtn.setVisibility(View.VISIBLE);
                        gestionarUsuarios.setVisibility(View.GONE);
                        break;
                    case "3":
                        jugarBtn.setVisibility(View.VISIBLE);
                        crearReglasBtn.setVisibility(View.GONE);
                        mantenimientoPreguntasBtn.setVisibility(View.GONE);
                        rankingBtn.setVisibility(View.VISIBLE);
                        gestionarUsuarios.setVisibility(View.GONE);
                        break;
                }
            } catch (NullPointerException ne) {
                System.out.println("No Type was gotten. we've been pranked.");
            }
        }
        public void Volver(View v){
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
    }
